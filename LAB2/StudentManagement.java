public class StudentManagement {

    public static boolean SameGroup(Student s1, Student s2 ) {
        if (s1.getGroup() == s2.getGroup())     return true;
        else return false;
    }

    public static void main(String[] args) {
        Student Sv = new Student();
        Sv.setName("Dang Tien Son");
        Sv.setMSSV("15020804");
        Sv.setGroup("K60CA");
        Sv.setEmail("thaklch@gmail.com");

        System.out.println("Name: " + Sv.getName());
        System.out.println("Info " + Sv.getInfo());

        Student s = new Student("Dang Tien Son", "hades_oval@gmail.com","15021802");
        System.out.println("Info  " + s.getInfo());

        Student v = new Student(Sv);
        System.out.println("Info  " + v.getInfo());

        Student s1 = new Student(Sv);
        Student s2 = new Student(Sv);
        System.out.println(Sv.getName()+ " va "+ s1.getName()+" cung lop " + SameGroup(Sv,s1));
        System.out.println(Sv.getName()+ " va "+ s.getName()+" cung lop " + SameGroup(Sv,s));

    }

}

