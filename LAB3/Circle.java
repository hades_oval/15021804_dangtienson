public class Circle extends Shape {
    private double radius;
    final double pi = 3.14;

    public Circle() {
        this(1);
    }   
    public Circle(double radius, String color, boolean filled) {
		super(color,filled);
        this.radius = radius;
    }
    public Circle(double radius) {
		super();
        this.radius = radius;
    }
	void setRadius(double radius){
		this.radius = radius;
	}
	double getRadius(){
		return radius;
	}
    double getArea() {
        return pi * radius * radius;
    }

    double getPerimeter() {
        return 2 * pi * radius;
    }
	public String toString(){
		return "Circle " + "Radius = " + radius +  " color = " + super.toString();

	}
}