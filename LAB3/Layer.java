import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
class Layer {
	private List<Shape> shapes = new ArrayList<Shape>();
	void addShape(Shape X){
		shapes.add(X);
	}
	public String toString(){
		String res = "";
		int i=1;
		for (Shape s: shapes){
			res += "Shape "+ i + ": " + s.toString() + "\n";
			i+=1;
		}
		return res;
	}
	void removeTriangle(){
		for(int i=0;i<shapes.size();i+=1)
			if (shapes.get(i) instanceof Triangle)
			{
				shapes.remove(shapes.get(i));
				i-=1;
			}
		
	}
	void removeCircle(){
		for(int i=0;i<shapes.size();i+=1)
			if (shapes.get(i) instanceof Circle)
			{
				shapes.remove(shapes.get(i));
				i-=1;
			}
		
	}
}
