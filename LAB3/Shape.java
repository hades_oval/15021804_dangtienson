public class Shape {
    public double area;
    public double perimeter;
	public String color = "red";
	public boolean filled = true;
	public Shape(String color, boolean filled){
		this.color = color;
		this.filled = filled;
	}
	public Shape(){
		this.color = "red";
		this.filled = true;
	}
	public String toString(){
		return "color = " + color + " filled = " + filled + ".";

	}	
}