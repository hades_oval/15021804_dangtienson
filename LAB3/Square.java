public class Square extends Rectangle {
    public Square() {
    }
    public Square(double side) {
		super(side,side);
    }
    public Square(double side, String color, boolean filled) {
        super(side,side,color,filled);
    }
	double getSide()	{
		return length;
	}
	void setSide(double side){
		this.length = side;
		this.width = side;
	}
    public double getArea() {
        return width * length;
    }

    public double getPerimeter() {
        return 2 * (width + length);
    }
	public String toString(){
		return "Square " + "side = " + width +  " color = " + super.toString();

	}
}