public class Triangle extends Shape {
    private double a, b, c; 

    public Triangle() {
        this(1,1,1);
    }
    public Triangle(double a, double b, double c) {
		super();
        this.a = a;
        this.b = b;
        this.c = c;
    }
	public Triangle(double a,double b,double c, String color, boolean filled) {
		super(color,filled);
		this.a = a;
        this.b = b;
        this.c = c;
    }
	void setA(double a){
		this.a = a; 
	}
	void setB(double b){
		this.b = b; 
	}
	void setC(double c){
		this.c = c; 
	}
	double getA(){
		return a;
	}
	double getB(){
		return b;
	}
	double getC(){
		return c;
	}

    double getArea() {
        double s = (a + b + c) / 2;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }

    double getPerimeter() {
        return a + b + c;
    }
	public String toString(){
		return "Triangle " + "a = " + a + " b = " + b + "c = " + c + super.toString();

	}
}