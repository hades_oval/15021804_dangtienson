import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
public class Diagram{
	private List<Layer> layers = new ArrayList<Layer>();
	void addLayer(Layer X){
		layers.add(X);
	}
	public String toString(){
		String res = "";
		int i=1;
		for (Layer s: layers){
			res += "Layer "+ i + ": \n" + s.toString() + "\n";
			i+=1;
		}
		return res;
	}
	void removeCircleInAllLayer(){
		for(Layer l : layers)
			l.removeCircle();
	}
}
