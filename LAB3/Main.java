class Main 
{
	public static void main(String[] args) 
	{
		Diagram d = new Diagram();
		Layer l = new Layer();
		l.addShape(new Circle(3.2,"green",true));
		l.addShape(new Square(1.0));
		l.addShape(new Triangle(2,5,6));
		d.addLayer(l);
		System.out.println(d.toString());
		l.removeTriangle();
		System.out.println("After remove Triangle:\n"+d.toString());
		d.removeCircleInAllLayer();
		System.out.println("After remove Circle:\n"+d.toString());
	}
}
