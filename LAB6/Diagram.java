import java.io.*;
import java.util.*;

public class Diagram implements Serializable {
	private List<Layer> layers = new ArrayList<Layer>();
	void addLayer(Layer X){
		layers.add(X);
	}
	public String toString(){
		String res = "";
		int i=1;
		for (Layer s: layers){
			res += "Layer "+ i + ": \n" + s.toString() + "\n";
			i+=1;
		}
		return res;
	}
	void removeCircleInAllLayer(){
		for(Layer l : layers)
			l.removeCircle();
	}
	void seperateShapetoLayer(){
        List<String> keyWords = new ArrayList<String>(Arrays.asList("Circle","Triangle","Rectangle","Square","Hexagon"));
		HashMap<String,Layer> hm = new HashMap<String,Layer>();
		for (String key : keyWords)
			hm.put(key, new Layer());

		for(Layer l:layers)
			for(Shape s:l.getShapes())
					hm.get(s.getClass().getSimpleName()).addShape(s);
		List<Layer> newLayers = new ArrayList<Layer>();
		for (String key: hm.keySet()){
			newLayers.add(hm.get(key));
		}
		layers = newLayers;
	}
	public static void writeToDisk(List<Diagram> listDiagram, String fileName) throws IOException {
        File f = new File(fileName);
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f));
        for (Diagram diagram : listDiagram)
            out.writeObject(diagram);
        out.close();
    }
    public static List<Diagram> readFromDisk(String fileName) throws IOException, ClassNotFoundException {
        File f = new File(fileName);
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
        List<Diagram> ListObject = new ArrayList<Diagram>();
        Diagram Object;
        while(true) {
            try {
                Object = (Diagram) in.readObject();
                if (Object != null) {
                    ListObject.add(Object);
                }
            } catch (EOFException e) {
                break;
            }
        }
        in.close();
        return ListObject;
    }

}
