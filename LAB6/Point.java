import java.io.Serializable;

/**
 * Created by CCNE on 20/07/2017.
 */
public class Point implements Serializable{
    double x,y;
    public  Point(double x,double y){
        this.x = x;
        this.y = y;
    }
    boolean isSame(Point a){
        if (x!=a.x || y != a.y) return false;
        return true;
    }
    double distance(Point a){
        return Math.sqrt((a.x-x)*(a.x-x)+(a.y-y)*(a.y-y));
    }
    double distanceSquare(Point a){
        return (a.x-x)*(a.x-x)+(a.y-y)*(a.y-y);
    }
    public  String toString(){
        return x+" "+ y +" ";
    }
}
