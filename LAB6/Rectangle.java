public class Rectangle extends Shape {
	public Rectangle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) {
		this(x1,y1,x2,y2,x3,y3,x4,y4,"red",true);
    }
    boolean isValid(){
    	int count=0;
		double a,b,c;
		boolean d=false;
    	for(Point p1:position)
    		for(Point p2:position)
    			for(Point p3:position)
    				for(Point p4:position)
    					if (p1!=p2 && p1!=p3 && p1!= p4 && p2!=p3 && p2!=p4 && p3!=p4) {
							if ((p1.x - p2.x == p3.x - p4.x && p1.y - p2.y == p3.y - p4.y) || (p1.x - p2.x == p4.x - p3.x && p1.y - p2.y == p4.y - p3.y)) {
								count++;
								//System.out.println(p1.toString() + p2 + p3 + p4);
							}
							a = p1.distanceSquare(p2);
							b = p1.distanceSquare(p3);
							c = p1.distanceSquare(p4);
							if (a == b + c || b == c + a || c == b + a) d = true;
						}
		//System.out.println(count + " " + d);
		if (count!=16 || d==false) return false;
		return true;
	}
	public Rectangle(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, String color, boolean filled) {
		super(color,filled);
		position.add(new Point(x1,y1));
		position.add(new Point(x2,y2));
		position.add(new Point(x3,y3));
		position.add(new Point(x4,y4));
	}
	double getWidth(){
		return Math.max(Math.abs(position.get(0).y-position.get(1).y),Math.abs(position.get(0).y-position.get(2).y));
	}
	double getLength(){
		return Math.max(Math.abs(position.get(0).x-position.get(1).x),Math.abs(position.get(0).x-position.get(2).x));
	}
	public String toString(){
		return "Rectangle " + "x1 = " + position.get(0).x + " y1 = " + position.get(0).y + "x2= " + position.get(1).x + " y2 = " + position.get(1).y + "x3 = " + position.get(2).x + " y3 = " + position.get(2).y + "x4 = " + position.get(3).x + " y4 = " + position.get(4).y + super.toString();
	}
}