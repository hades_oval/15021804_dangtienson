/**
 * Created by Dang Tien Son on 7/30/2017.
 */
import static java.lang.Double.max;

public class Hexagon extends Shape {
    public Hexagon(double x1, double y1, double x2, double y2, double x3, double y3, String color, boolean filled) {
        position.add(new Point(x1,y1));
        position.add(new Point(x2,y2));
        position.add(new Point(x3,y3));

    }
    boolean isValid(){
        Point p1 = position.get(0), p2 = position.get(1), p3 = position.get(2);
        if (angleBetweenThreePoint(p1,p2,p3)==true) return true;
        if (angleBetweenThreePoint(p2,p1,p3)==true) return true;
        if (angleBetweenThreePoint(p3,p2,p1)==true) return true;
        return false;
    }
    private boolean angleBetweenThreePoint(Point a,Point b,Point c){
        double ux = a.x-b.x, uy = a.y-b.y, vx = c.x-b.x, vy = c.y-b.y;
        double numerator  = ux*vx+uy*vy;
        double denominator = Math.sqrt(ux*ux+uy*uy)*Math.sqrt(vx*vx+vy*vy);
        if (Math.abs(numerator/denominator - Math.cos(120)) <= 1e-9)
            return true;
            else
                return false;

    }
    public Hexagon(double x1, double y1, double x2, double y2, double x3, double y3){
        this(x1,y1,x2,y2,x3,y3,"res",true);
    }
    public String toString(){
        return "Hexagon " + "x1 = " + position.get(0).x + " y1 = " + position.get(0).y + "x2= " + position.get(1).x + " y2 = " + position.get(1).y + "x3 = " + position.get(2).x + " y3 = " + position.get(2).y + "x4 = " + position.get(3).x + " y4 = " + position.get(4).y + super.toString();
    }
}