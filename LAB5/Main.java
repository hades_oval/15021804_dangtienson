class Main 
{
	public static void main(String[] args) 
	{
		Diagram d = new Diagram();
		Layer l = new Layer();
		l.addShape(new Circle(1,1,3.2,"green",true));
		l.addShape(new Square(-1,0,1,0,0,-1,0,1));
		l.addShape(new Triangle(1,1,2,2,3,3));

		Layer l1 = new Layer();
		l1.addShape(new Circle(1,1,3.2,"green",true));
		l1.addShape(new Square(-1,0,1,0,0,-1,0,2));
		l1.addShape(new Triangle(1,1,2,2,3,4));

		d.addLayer(l);
		d.addLayer(l1);
		System.out.println(d.toString());
		d.seperateShapetoLayer();
		System.out.println(d.toString());

	}
}
