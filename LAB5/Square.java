import static java.lang.Double.max;

public class Square extends Rectangle {
    public Square(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, String color, boolean filled) {
        super(x1,y1,x2,y2,x3,y3,x4,y4,color,filled);
        position.add(new Point(x1,y1));
        position.add(new Point(x2,y2));
        position.add(new Point(x3,y3));
        position.add(new Point(x4,y4));
    }
    public Square(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4){
        this(x1,y1,x2,y2,x3,y3,x4,y4,"res",true);
    }
    double getSide(){
        return super.getWidth();
    }
    public String toString(){
        return "Square " + "x1 = " + position.get(0).x + " y1 = " + position.get(0).y + "x2= " + position.get(1).x + " y2 = " + position.get(1).y + "x3 = " + position.get(2).x + " y3 = " + position.get(2).y + "x4 = " + position.get(3).x + " y4 = " + position.get(4).y + super.toString();
    }
}