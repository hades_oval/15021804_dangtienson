import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
class Layer {
	boolean visible;
	private List<Shape> shapes = new ArrayList<Shape>();
	void addShape(Shape X){
		shapes.add(X);
	}
	public String toString(){
		String res = "";
		int i=1;
		for (Shape s: shapes){
			res += "Shape "+ i + ": " + s.toString() + "\n";
			i+=1;
		}
		return res;
	}
	List<Shape> getShapes(){
		return shapes;
	}
	void removeTriangle(){
		for(int i=0;i<shapes.size();i+=1)
			if (shapes.get(i) instanceof Triangle)
			{
				shapes.remove(shapes.get(i));
				i-=1;
			}
		
	}
	void removeCircle(){
		for(int i=0;i<shapes.size();i+=1)
			if (shapes.get(i) instanceof Circle)
			{
				shapes.remove(shapes.get(i));
				i-=1;
			}
		
	}
	void removeSame(){
		int i,j;
		boolean[] d = new boolean[1000];
		Shape a,b;
		for(i=0;i<shapes.size();i++)
			if (d[i]==false)
			for(j=i+1;j<shapes.size();j++){
				a=shapes.get(i);
				b=shapes.get(j);
				if (a.isSame(b)) d[j]=true;
			}
		for(i=0;i<shapes.size();i+=1)
			if (d[i]==true)
			{
				shapes.remove(shapes.get(i));
				i-=1;
			}
	}
}
