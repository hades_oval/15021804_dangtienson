public class Triangle extends Shape {
    public Triangle(double x1, double y1, double x2, double y2, double x3, double y3) {
		this(x1,y1,x2,y2,x3,y3,"red",true);
    }
	public Triangle(double x1, double y1, double x2, double y2, double x3, double y3, String color, boolean filled) {
		super(color,filled);
		position.add(new Point(x1,y1));
		position.add(new Point(x2,y2));
		position.add(new Point(x3,y3));
    }
    public  boolean isValid(){
    	Point p1=position.get(0);
		Point p2=position.get(1);
		Point p3=position.get(2);
		if ((p1.x-p2.x == p2.x-p3.x && p1.y-p2.y==p2.y-p3.y) || (p1.x-p2.x == -p2.x+p3.x && p1.y-p2.y==-p2.y+p3.y)) return false;
		return true;

	}
	public String toString(){
		return "Triangle " + "x1 = " + position.get(0).x + " y1 = " + position.get(0).y + "x2= " + position.get(1).x + " y2 = " + position.get(1).y + "x3 = " + position.get(2).x + " y3 = " + position.get(2).y + super.toString();
	}
}