public class Circle extends Shape {
    private double radius;
    final double pi = 3.14;
    public Circle(double x, double y, double radius, String color, boolean filled) {
		super(color,filled);
        this.radius = radius;
        position.add(new Point(x,y));
		position.add(new Point(0,radius));
    }
    public boolean isValid(){
    	return true;
	}
    public Circle(double x, double y, double radius) {
		this(x,y,radius,"red",true);
    }
	void setRadius(double radius){
		this.radius = radius;
	}
	double getRadius(){
		return radius;
	}
	public String toString(){
		return "Circle " + "Radius = " + radius +  " color = " + super.toString();

	}
}