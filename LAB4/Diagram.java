import java.util.*;

public class Diagram{
	private List<Layer> layers = new ArrayList<Layer>();
	void addLayer(Layer X){
		layers.add(X);
	}
	public String toString(){
		String res = "";
		int i=1;
		for (Layer s: layers){
			res += "Layer "+ i + ": \n" + s.toString() + "\n";
			i+=1;
		}
		return res;
	}
	void removeCircleInAllLayer(){
		for(Layer l : layers)
			l.removeCircle();
	}
	void seperateShapetoLayer(){
        List<String> keyWords = new ArrayList<String>(Arrays.asList("Circle","Triangle","Rectangle","Square"));
		HashMap<String,Layer> hm = new HashMap<String,Layer>();
		for (String key : keyWords)
			hm.put(key, new Layer());

		for(Layer l:layers)
			for(Shape s:l.getShapes())
					hm.get(s.getClass().getSimpleName()).addShape(s);
		List<Layer> newLayers = new ArrayList<Layer>();
		for (String key: hm.keySet()){
			newLayers.add(hm.get(key));
		}
		layers = newLayers;
	}
}
