import java.util.ArrayList;

 public abstract class Shape {
	protected String color = "red";
	protected boolean filled = true;
	ArrayList<Point> position = new ArrayList<Point>();
	abstract boolean isValid();
	public Shape(String color, boolean filled){
		this.color = color;
		this.filled = filled;
	}
	public Shape(){
		this.color = "red";
		this.filled = true;
	}
	boolean isSame(Shape a){
		if (this.getClass() != a.getClass()) return false;
		boolean d =false;
		for (Point i : position) {
			for (Point j : a.position)
				if (i.isSame(j)) d = true;
			if (!d) return false;
			d = false;
		}
		for (Point i : a.position) {
			for (Point j : position)
				if (i.isSame(j)) d = true;
			if (!d) return false;
			d = false;
		}
		return true;
	}
	String getColor()	{
		return color;
	}
	void setColor(String color){
		this.color = color;
	}
	boolean getFilled()	{
		return filled;
	}
	void setFilled(boolean filled){
		this.filled = filled;
	}
	public String toString(){
		return "color = " + color + " filled = " + filled + ".";
	}	
}